package exercise;


import java.util.HashMap;
import java.util.Map;

// BEGIN
class FileKV extends Utils implements KeyValueStorage {
    private String path;
    private Map<String, String> dictionary = new HashMap<>();

    FileKV(String pathFile, Map<String, String> map) {
        this.path = pathFile;
        this.dictionary = new HashMap<>(map);
    }

    @Override
    public void set(String key, String value) {
        Utils.readFile(path);
        dictionary.put(key, value);
    }

    @Override
    public void unset(String key) {
        dictionary.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        for (Map.Entry<String, String> string : dictionary.entrySet()) {
            if (string.getKey().equals(key)) {
                return string.getValue();
            }
        }
        return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(dictionary);
    }

    @Override
    public String toString() {
        return "FileKV{" + "path='" + path + '\''
                + ", dictionary=" + dictionary
                + '}';
    }

    public static void main(String[] args) {
        KeyValueStorage storage = new FileKV("src/test/resources/file",
                Map.of("key", "value"));
        System.out.println(storage);
    }
}

// END

// BEGIN
