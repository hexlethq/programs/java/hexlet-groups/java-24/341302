package exercise;


import java.util.Map;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static void swapKeyValue(KeyValueStorage keyValueStorage) {
        Map<String, String> buffer = keyValueStorage.toMap();
        Map<String, String> newMap =
                buffer.entrySet().stream()
                        .collect(Collectors
                        .toMap(Map.Entry::getValue, Map.Entry::getKey));

        for (Map.Entry<String, String> entry : newMap.entrySet()) {
            keyValueStorage.unset(entry.getValue());
            for (Map.Entry<String, String> entry1 : newMap.entrySet()) {
                keyValueStorage.set(entry1.getKey(), entry1.getValue());
            }
        }
    }

}
// END
