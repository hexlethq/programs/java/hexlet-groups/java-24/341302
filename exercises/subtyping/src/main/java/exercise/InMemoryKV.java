package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class InMemoryKV implements KeyValueStorage {
  private final Map<String, String> dictionary = new HashMap<>();

    InMemoryKV(Map<String, String> map) {
        dictionary.putAll(map);
    }

    @Override
    public void set(String key, String value) {
        if (dictionary.containsKey(key)) {
            dictionary.replace(key, value);
        } else {
            dictionary.put(key, value);
        }
    }

    @Override
    public void unset(String key) {
        dictionary.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        for (Map.Entry<String, String> string : dictionary.entrySet()) {
            if (string.getKey().equals(key)) {
                return string.getValue();
            }
        }
        return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {

        return new HashMap<>(dictionary);
    }

    @Override
    public String toString() {
        return "InMemoryKV{" + "dictionary=" + dictionary + '}';
    }
}
