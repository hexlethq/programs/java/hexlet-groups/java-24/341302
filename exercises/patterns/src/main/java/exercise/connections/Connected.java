package exercise.connections;

import exercise.TcpConnection;

class Connected implements Connection {

    public TcpConnection tcpConnection;

    Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error");
    }

    @Override
    public void disconnect() {
        TcpConnection tcpConnection = this.tcpConnection;
        tcpConnection.setConnection(new Disconnected(tcpConnection));
        System.out.println("disconnected");
    }

    @Override
    public void write(String string) {
        if (this.tcpConnection.getCurrentState().equals("disconnect")) {
            System.out.println("Error");
        } else {
            System.out.println(string);
        }

    }
}
// BEGIN

// END
