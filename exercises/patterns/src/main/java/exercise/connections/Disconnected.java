package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection {

    private final TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public String getCurrentState() {
        return "disconnected";
    }

    @Override
    public void connect() {
        TcpConnection tcp = this.tcpConnection;
        tcp.setConnection(new Connected(tcp));
        System.out.println("connected");
    }

    @Override
    public void disconnect() {
        System.out.println("Error");
    }

    @Override
    public void write(String string) {
        if(this.tcpConnection.getCurrentState().equals("disconnected")) {
            System.out.println("Error");
        } else {
            System.out.println(string);
        }

    }
}
// END
