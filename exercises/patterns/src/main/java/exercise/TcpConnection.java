package exercise;

import exercise.connections.Connection;
import exercise.connections.Disconnected;

import java.util.List;
import java.util.ArrayList;

public class TcpConnection implements Connection {
    Connection connection;
    int port;
    String ip;

    public TcpConnection(String string, int i) {
        this.ip = string;
        this.port = i;
        this.connection = new Disconnected(this);
    }

    // BEGIN
    public void setConnection(Connection connect) {
        this.connection = connect;
    }


    public String getCurrentState() {
        return this.connection.getCurrentState();
            }

    public void connect() {
        this.connection.connect();
    }

    public  void disconnect() {
        this.connection.disconnect();

    }

    public  void write(String string) {
        this.connection.write(string);
    }
// END

}

