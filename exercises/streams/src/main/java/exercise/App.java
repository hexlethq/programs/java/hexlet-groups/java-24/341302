package exercise;

import java.util.ArrayList;
import java.util.Arrays;

// BEGIN7
class App {


    public static int getCountOfFreeEmails(ArrayList<String> list) {
        String[] wer = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            wer[i] = list.get(i).split("@")[1];
        }
        ArrayList<String> email = new ArrayList<>(Arrays.asList(wer));
        return (int) email.stream()
                .filter(x -> x.equals("gmail.com")
                        || x.equals("yandex.ru") || x.equals("hotmail.com"))
                .count();
    }

    public static void main(String[] args) {
        String[] emails = {
            "info@gmail.com",
            "info@yandex.ru",
            "mk@host.com",
            "support@hexlet.io",
            "info@hotmail.com",
            "support.yandex.ru@host.com"
        };

        ArrayList<String> emailsList = new ArrayList<>(Arrays.asList(emails));
        System.out.println((App.getCountOfFreeEmails(emailsList))); // 3
    }
}
// END
