package exercise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;


// BEGIN
class App {

    public static String getForwardedVariables(String data2)  {
        String [] array = data2.split("\n");
        String str = Arrays.stream(array).filter(st-> st.contains("en"))
        .flatMap(y->Arrays.stream(y.split("\""))).filter(y -> y.contains("D_"))
        .flatMap(z->Arrays.stream(z.split(","))).filter(z->z.contains("D_"))
        . map(x->x.replace("X_FORWARDED_",""))
        .collect((Collectors.joining(",")));
      //  System.out.println(str);
return str;
    }

  private static Path getFixturePath(String fileName) {
      return Paths.get("src", "test", "resources", "fixtures", fileName)
              .toAbsolutePath().normalize();
  }

    public static void main(String[] args) throws IOException {
        getForwardedVariables(Files.readString(getFixturePath("s2.conf")));
    }

//END
}