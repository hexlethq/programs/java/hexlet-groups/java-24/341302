package exercise;

class App {
    // BEGIN
    public static  String getTypeOfTriangle(int a, int b, int c) {
        if (a == b && b == c) {
           return "Равносторонний";
                }
        if  (a == b  ||  a == c ||  b == c) {
            return "Равнобедренный";
        }
        if (a + b  >= c && c + b >= a && a + c >= b) {
            return "Разносторонний";
        } else {
            return "Треугольник не существует";
        }
    }

    public static void main(String[] args) {
        System.out.println(getTypeOfTriangle(5, 6, 5));
    }
    // END
}
