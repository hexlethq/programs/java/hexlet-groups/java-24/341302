<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN -->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Delete user</title>
</head>
<body>
<h1>Delete user ${user.get("firstName")} ${user.get("lastName")}?</h1>
<form action="/users/delete?id=${user.get("id")}" method="post">
    <button type="submit" class="btn btn-danger">DELETE</button>
</form>
</body>
</html>
