package exercise;

class Converter {
    // BEGIN
    public static  int convert(int a, String s) {
        if (s.equals("b")) {
            System.out.println(a + " " + "Kb" + " " + "=" + " " + a * 1024 + " " + "b");
            return a * 1024;
        }
        if (s.equals("Kb")) {
            System.out.println(a + " " + "b" + " " + "=" + " " + a / 1024 + " " + "Kb");
            return a / 1024;
        }
        return 0;
    }

    public static void main(String[] args) {
        convert(10, "b");
    }
    // END
}
