package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int c) {
        double rad = c * 3.14 / 180;
        return 0.5 * a * b * Math.sin(rad);
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
