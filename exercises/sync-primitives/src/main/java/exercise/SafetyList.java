package exercise;

import java.util.ArrayList;
import java.util.List;

class SafetyList {
    // BEGIN
    private List<Integer> base = new ArrayList<>();

    public synchronized void add(Integer num) {
        base.add(num);
    }

    public Integer get(int index) {
        return base.get(index);
    }

    public int getSize() {
        return base.size();
    }
    // END
}
