package exercise;

// BEGIN
public class ListThread extends Thread {

    private SafetyList base;

    public ListThread(SafetyList base) {
        this.base = base;
    }

    @Override
    public void run() {
        for (int i = 0; i != 1000; i++) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            base.add(i);
        }
    }
}

// END
