package exercise;

// BEGIN
class Segment {

    private Point begin;
    private Point end;

    Segment(Point begin, Point end) {
        this.begin = begin;
        this.end = end;
    }

    public Point getMidPoint() {
        int middleX = (this.begin.getX() + this.end.getX()) / 2;
        int middleY = (this.begin.getY() + this.end.getY()) / 2;
        return new Point(middleX, middleY);
    }

    public Point getBeginPoint() {
        return begin;
    }

    public Point getEndPoint() {
        return end;
    }

}
// END
