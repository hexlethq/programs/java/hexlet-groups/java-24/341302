package exercise;

import java.time.LocalDateTime;

import exercise.daytimes.Daytime;
import exercise.daytimes.Morning;
import exercise.daytimes.Day;
import exercise.daytimes.Evening;
import exercise.daytimes.Night;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// BEGIN
@Configuration
public class MyApplicationConfig {
    @Bean("daytime-for-meal")
    public Daytime initBean() {
        LocalDateTime localDateTime = LocalDateTime.now();
        int currentHour = localDateTime.getHour();
        if (currentHour >= 6 && currentHour <= 11) {
            return new Morning();
        }
        if (currentHour >= 12 && currentHour <= 17) {
            return new Day();
        }
        if (currentHour >= 18 && currentHour <= 22) {
            return new Evening();
        }
        return new Night();
    }
}

// END
