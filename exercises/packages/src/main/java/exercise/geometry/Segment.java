// BEGIN
package exercise.geometry;

public class Segment {
    private double[] point1;
    private double[] point2;

    public Segment(double[] point1, double[] point2) {
        this.point1 = point1;
        this.point2 = point2;
    }
    public static Segment makeSegment(double[] point1, double[] point2) {
        return new Segment(point1, point2);
    }
    public static double[] getBeginPoint(Segment seg) {
        return seg.getPoint1();
    }

    public static double[] getEndPoint(Segment seg) {
        return seg.getPoint2();
    }

    public double[] getPoint1() {
        return point1;
    }

    public void setPoint1(double[] point1) {
        this.point1 = point1;
    }

    public double[] getPoint2() {
        return point2;
    }

    public void setPoint2(double[] point2) {
        this.point2 = point2;
    }
 }




















// END
