// BEGIN
package exercise;
import  exercise.geometry.Segment;
import exercise.geometry.Point;
import java.util.Arrays;

public class App {
    public static double[] getMidpointOfSegment(Segment seg) {
        double y1 = Point.getY(Segment.getBeginPoint(seg));
        double x1 = Point.getX(Segment.getBeginPoint(seg));
        double y2 = Point.getY(Segment.getEndPoint(seg));
        double x2 = Point.getX(Segment.getEndPoint(seg));
        double x = ((x1 + x2) / 2);
        double y = ((y1 + y2) / 2);
        return Point.makePoint(x, y);
    }

    public static Segment reverse(Segment seg) {
        double[] point2 = Arrays.copyOf(Segment.getBeginPoint(seg), 2);
        double[] point1 = Arrays.copyOf(Segment.getEndPoint(seg), 2);
        return new Segment(point1, point2);
    }

    public static boolean isBelongToOneQuadrant(Segment seg) {
        double xBegin = Point.getX(Segment.getBeginPoint(seg));
        double yBegin = Point.getY(Segment.getBeginPoint(seg));
        double xEnd = Point.getX(Segment.getEndPoint(seg));
        double yEnd = Point.getY(Segment.getEndPoint(seg));
        if (xBegin > 0 && xEnd > 0 || xBegin < 0 && xEnd < 0) {
          return yBegin > 0 && yEnd > 0 || yBegin < 0 && yEnd < 0;
        }
        return false;
    }

     public static void main(String[] args) {
         double[] point1 = Point.makePoint(3, 2);
         double[] point2 = Point.makePoint(1, 1);
         Segment seg = new Segment(point1, point2);
         Segment.getBeginPoint(seg);
         Segment.getEndPoint(seg);
       //  System.out.println(Arrays.toString(getMidpointOfSegment(seg)));
         System.out.println(isBelongToOneQuadrant(seg));
     }
}

// END
