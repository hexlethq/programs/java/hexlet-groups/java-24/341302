// BEGIN
package exercise;
import com.google.gson.Gson;

class App {
   public static String toJson(String[] fruits) {
       Gson gson = new Gson();
       return gson.toJson(fruits);
   }

   public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
// END
