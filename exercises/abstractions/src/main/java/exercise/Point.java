package exercise;

import java.util.Arrays;

class Point {
    // BEGIN
  public static  int[] makePoint(int x, int y) {
   int[] arr = new int[2];
   arr[0] = x;
   arr[1] = y;
   return  arr;
  }
   public static int getX(int[] arr) {
   return arr[0];
   }
    public static int getY(int[] arr) {
    return arr[1];
    }
   public static String pointToString(int[] arr) {
    return "(" +  arr[0] + "," + " " + arr[1] + ")";
   }
    public static int getQuadrant(int[] arr) {
    if (arr[0] == 0 || arr[1] == 0) {
     return 0;
    }
     if (arr[0] > 0 && arr[1] > 0) {
      return 1;
     }
     if (arr[0] < 0 && arr[1] > 0) {
      return 2;
     }
     if (arr[0] > 0 && arr[1] < 0) {
      return 4;
     }
      return 3;
    }

 public static int[] getSymmetricalPointByX(int[] arr) {
   arr[1] = -arr[1];
   return arr;
 }
 public static double calculateDistance(int[] arr1, int[] arr2) {
   double x = Math.pow(arr1[0] - arr2[0], 2);
   double y = Math.pow((arr1[1] - arr2[1]), 2);
  return Math.sqrt(x + y);
 }
 public static void main(String[] args) {
    var point = makePoint(-2, 3);
    System.out.println(pointToString(point));
    System.out.println(getQuadrant(point));
    System.out.println(Arrays.toString(getSymmetricalPointByX(point)));
     var point1 = makePoint(0, 0);
     var point2 = makePoint(-3, -4);
     System.out.println(calculateDistance(point1, point2));
}
    // END
}
