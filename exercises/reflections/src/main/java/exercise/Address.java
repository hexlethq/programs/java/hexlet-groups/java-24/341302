package exercise;

class Address {
    // BEGIN
    @NotNull
    @MinLength(min = 4)
    // END
    private String country;

    // BEGIN
    @NotNull
    // END
    private String city;

    // BEGIN
    @NotNull
    // END
    private String street;

    // BEGIN
    @NotNull
    // END
    private String houseNumber;

    private String flatNumber;

    Address(String country1, String city1, String street1,
            String houseNumber1, String flatNumber1) {
        this.country = country1;
        this.city = city1;
        this.street = street1;
        this.houseNumber = houseNumber1;
        this.flatNumber = flatNumber1;
    }
}
