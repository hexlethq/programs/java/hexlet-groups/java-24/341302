package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
class Validator {


    public static List<String> validate(Address address)
            throws IllegalAccessException {
        List<String> list = new ArrayList<>();
        Field[] field = address.getClass().getDeclaredFields();
        for (Field fields : field) {
            NotNull str = fields.getAnnotation(NotNull.class);
            if (str != null) {
                fields.setAccessible(true);
                if (fields.get(address) == null) {
                    list.add(fields.getName());
                }
            }
        }
        return list;
    }

    public static Map<String, List<String>> advancedValidate(Address address)
            throws IllegalAccessException {
        Map<String, List<String>> map = new HashMap<>();

        Field[] field = address.getClass().getDeclaredFields();
        for (Field fields : field) {
            List<String> list = new ArrayList<>();
            NotNull str = fields.getAnnotation(NotNull.class);
            MinLength length = fields.getAnnotation(MinLength.class);
            fields.setAccessible(true);

            if (str != null) {
                if (fields.get(address) == null) {
                    list.add("null");
                    map.put(fields.getName(), list);
                }
            }
            if (length != null) {
                String str1 = (String) (fields.get(address));
                if (str1.length() < length.min()) {
                    list.add("less than 4");
                    map.put(fields.getName(), list);
                }
            }
        }
        return map;
    }

    public static void main(String[] args) throws IllegalAccessException {

    }
}

