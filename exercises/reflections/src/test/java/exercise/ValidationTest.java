package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Map;



class ValidationTest {

    @Test
    void testValidate() throws IllegalAccessException {
        Address address1 = new Address("Russia", "Ufa", "Lenina", "54", null);
        List<String> result1 = Validator.validate(address1);
        List<String> expected1 = List.of();
        assertThat(result1).isEqualTo(expected1);

        Address address2 = new Address(null, "London", "1-st street", "5", "1");
        List<String> result2 = Validator.validate(address2);
        List<String> expected2 = List.of("country");
        assertThat(result2).isEqualTo(expected2);

        Address address3 = new Address("USA", null, null, null, "1");
        List<String> result3 = Validator.validate(address3);
        List<String> expected3 = List.of("city", "street", "houseNumber");
        assertThat(result3).isEqualTo(expected3);
    }

    // BEGIN
    @Test
    void testAdvanceValidate1() throws IllegalAccessException {
        Address address11 = new Address("GB", "SDFADSFASF", null, null, "12");
        Map<String, List<String>> res = Validator.advancedValidate(address11);
        Map<String, List<String>> exp = Map.of("country", List.of("less than 4"),
                "street", List.of("null"),
                "houseNumber", List.of("null"));
        assertThat(res).isEqualTo(exp);
    }
        // END
}
