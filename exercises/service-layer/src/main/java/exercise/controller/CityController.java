package exercise.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.dto.WeatherDto;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;


@RestController
public class CityController {

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public WeatherDto getCityById(@PathVariable(name = "id") long id) throws JsonProcessingException {
        return weatherService.getCityById(id);
    }

    @GetMapping(path = "/search")
    public List<WeatherDto> getCities(@RequestParam(name = "name", required = false) String name) throws JsonProcessingException {
        return weatherService.getCities(name);
    }
    // END
}

