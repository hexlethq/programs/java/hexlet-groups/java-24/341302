package exercise.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WeatherDto {
    private String temperature;
    private String name;
    private String cloudy;
    private String wind;
    private String humidity;

    public WeatherDto(String temperature, String name) {
        this.temperature = temperature;
        this.name = name;
    }

}
