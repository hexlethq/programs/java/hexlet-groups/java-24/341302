package exercise.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.HttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import exercise.dto.WeatherDto;
import org.springframework.stereotype.Service;
import exercise.CityNotFoundException;
import exercise.repository.CityRepository;
import exercise.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@Service
public class WeatherService {

    @Autowired
    CityRepository cityRepository;

    // Клиент
    HttpClient client;

    private ObjectMapper objectMapper = new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    // BEGIN
    public String getWeather(String cityName) {
        return client.get("http://weather/api/v2/cities/" + cityName);
    }

    public WeatherDto getCityById(@PathVariable(name = "id") long id) throws JsonProcessingException {
        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City with such ID not found"));

        return objectMapper.readValue(getWeather(city.getName()), WeatherDto.class);
    }

    public List<WeatherDto> getCities(@RequestParam(name = "name", required = false) String name) throws JsonProcessingException {
        List<WeatherDto> weatherDtos = new ArrayList<>();
        if (name != null) {
            List<City> cities =  cityRepository.findAllByNameStartsWithIgnoreCase(name);

            for (City c : cities) {
                weatherDtos.add(objectMapper.readValue(getWeather(c.getName()), WeatherDto.class));
            }
            return weatherDtos;
        }

        List<City> cities = cityRepository.findAllByOrderByNameAsc();

        for (City c : cities) {
            weatherDtos.add(objectMapper.readValue(getWeather(c.getName()), WeatherDto.class));
        }

        return weatherDtos;
    }
    // END
}
