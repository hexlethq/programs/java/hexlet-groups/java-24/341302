package exercise;

import java.util.Arrays;

// BEGIN
public class MaxThread extends Thread {
    private int[] arr;
    private static Integer max;

    public MaxThread(int[] arr) {
        this.arr = arr;
    }

    @Override
    public void run() {
        max = Arrays.stream(arr)
                .max().getAsInt();
    }

    public static Integer getMax() {
        return max;
    }
}

// END
