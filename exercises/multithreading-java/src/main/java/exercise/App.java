package exercise;

import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;




class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static Map<String, Integer> getMinMax(int[] arr) {
        Thread minThread = new MinThread(arr);
        Thread maxThread = new MaxThread(arr);
        minThread.start();
        maxThread.start();
        try {
            minThread.join();
            maxThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int min = MinThread.getMin();
        int max = MaxThread.getMax();

        return Map.of("min", min,
                "max", max);
    }
    // END
}

