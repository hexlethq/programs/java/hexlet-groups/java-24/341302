package exercise;

import java.util.stream.Collectors;
import java.util.Map;

class Tag {

    private final String name;
    private final Map<String, String> attributes;

    Tag(String nam, Map<String, String> attribute) {
        this.name = nam;
        this.attributes = attribute;
    }

    public String stringifyAttributes() {
        return attributes.keySet().stream()
            .map(key -> {
                String value = attributes.get(key);
                return String.format(" %s=\"%s\"", key, value);
            })
            .collect(Collectors.joining(""));
    }

    public String getName() {
        return name;
    }
}
