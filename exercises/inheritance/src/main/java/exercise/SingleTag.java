package exercise;

import java.util.Map;

// BEGIN
class SingleTag extends Tag {

    SingleTag(String img, Map<String, String> tag) {
        super(img, tag);
    }

    @Override
    public String stringifyAttributes() {
        return super.stringifyAttributes();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String toString() {
        return "<" + getName() + stringifyAttributes() + ">";
    }
}
// END
