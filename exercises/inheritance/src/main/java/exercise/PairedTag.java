package exercise;

import java.util.Map;
import java.util.List;


// BEGIN
class PairedTag extends Tag {
    private final String tagBody;
    private final List<Tag> list;


    PairedTag(String name, Map<String, String> attributes,
              String tagBody, List<Tag> singleTag) {
        super(name, attributes);
        this.tagBody = tagBody;
        this.list = singleTag;
    }

    @Override
    public String stringifyAttributes() {
        return super.stringifyAttributes();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<").append(getName()).append(stringifyAttributes()).append(">");
        if (list.size()==0) {
            stringBuilder.append(tagBody);
        }
        for (Tag singletag : list) {
            stringBuilder.append("<").append(singletag.getName())
                    .append(singletag.stringifyAttributes())
                    .append(">").append(tagBody);
        }
        return stringBuilder.append("</").append(getName()).append(">") + "";
    }

}
// END
