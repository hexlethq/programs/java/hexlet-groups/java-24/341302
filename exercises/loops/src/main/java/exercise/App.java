package exercise;
import java.util.Locale;

class App {
    // BEGIN
  public static String getAbbreviation(String s) {
      char[] st = s.toCharArray();
      StringBuilder res = new StringBuilder();
      for (int i = 0; i < st.length; i++) {
          if (Character.isUpperCase(st[i])) {
              res.append(st[i]);
          }
          if (st[i] == 32 && Character.isLowerCase(st[ i + 1 ])) {
              res.append(st[i + 1]);
          }
      }
      return res.toString().toUpperCase(Locale.ROOT);
  }
    public static void main(String[] args) {
        System.out.println(getAbbreviation("AsssAdffA"));

    }
    // END
}
