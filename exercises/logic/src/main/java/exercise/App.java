package exercise;



class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
      boolean isBigOddVariable;
        isBigOddVariable = number >= 1010 && number % 2 != 0;
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 0) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
     if (minutes <= 14)   {
         System.out.println("First");
     }
     if (minutes > 14 && minutes <= 30) {
         System.out.println("Second");
     }
     if (minutes > 30 && minutes <= 45) {
         System.out.println("Third");
     }
     if (minutes > 45) {
         System.out.println("Fourth");
     }
        // END
    }
}
