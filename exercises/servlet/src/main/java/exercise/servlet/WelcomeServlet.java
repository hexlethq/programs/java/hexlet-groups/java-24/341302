package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// hexlet program init --gitlab-group-id=11827568 --hexlet-user-id=341302 --gitlab-token=glpat-XsXLyLfw3dFx5ped4iZx
// BEGIN
@WebServlet
public class WelcomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter = resp.getWriter();
        printWriter.write("Hello, Hexlet!");
        System.out.println("");
    }
}
// END
