package exercise;


import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
     if (arr == null) {
         return null;
     }
     for (int i = 0; i < arr.length / 2; i++) {
           int tmp = arr[i];
           arr[i] = arr[arr.length - i - 1];
           arr[arr.length - 1 - i] = tmp;
     }
    return arr;
    }

    public  static  int getIndexOfMaxNegative(int[] arr) {
        if (arr == null) {
            return -1;
        }

        int count = 0;
        for (int j : arr) {
            if (j < 0) {
                count++;
            }
        }
        if (count == 0) {
            return -1;
        }
      int temp1 = Integer.MIN_VALUE;
      int index = 0;
      for (int i = 0; i < arr.length - 1; i++) {
          if (arr[i] == arr[i + 1] && arr[i] < 0) {
                  index = i;
          }
          if (arr[i] < 0  && arr[i] > temp1) {
              temp1 = arr[i];
              index = i;
          }
      }
       return  index;
    }
    public static void main(String[] args) {
       int[] arr1 = {1, 7, 23, 15, 0, 5, 8};
        int[] arr2 = {-30, 42, -5, 31, -37, 25, -50};
       System.out.println(Arrays.toString(reverse(arr1)));
        System.out.println(getIndexOfMaxNegative(arr2));
    }
    // END
}
