package exercise;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.CompletableFuture;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;

class App {

    // BEGIN
    public static CompletableFuture<String> unionFiles(String path1,
                                                       String path2,
                                                       String dest) throws ExecutionException, InterruptedException {
        CompletableFuture<String> path1Res = readFile(path1);
        CompletableFuture<String> path2Res = readFile(path2);

        path1Res.get();
        path2Res.get();

        return path1Res.thenCombine(path2Res, (firstFile, secondFile) -> {
            String fileName = Paths.get(dest).toAbsolutePath().normalize().toString();
            File destFile = new File(fileName);

            if (!destFile.exists()) {
                try {
                    destFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                FileWriter fw = new FileWriter(fileName);
                PrintWriter pw = new PrintWriter(fw);
                pw.print(firstFile);
                pw.print(secondFile);
                fw.close();
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        });
    }

    private static CompletableFuture<String> readFile(String path) {
        return CompletableFuture.supplyAsync(() -> {
            String result = "";

            try {
                result = Files.readString(Paths.get(path).toAbsolutePath().normalize());
            } catch (IOException e) {
                System.out.println(e);
            }

            return result;
        });
    }

    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        CompletableFuture<String> result = unionFiles("src/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/result.txt");
        // END
    }
}

