package exercise;

public class Kennel {
    private static String[][] pitomnik = new String[100][2];

    public static void addPuppy(String[] dog) {
        Dog one = new Dog(dog[0], dog[1]);
        for (int i = 0; i < pitomnik.length; i++) {
            if (pitomnik[i][0] == null) {
                pitomnik[i][0] = one.getName();
                pitomnik[i][1] = one.getPoroda();
                break;
            }
        }
    }

    public static void addSomePuppies(String[][] dogs) {
        int numPuppies = getPuppyCount();
        for (int i = numPuppies - 1; i < dogs.length; i++) {
            addPuppy(dogs[i]);
        }
    }

    public static int getPuppyCount() {
        int count = 0;
        for (String[] strings : pitomnik) {
            if (strings[0] != null) {
                count++;
            }
        }
        return count;
    }

    public static boolean isContainPuppy(String dogname) {
        for (String[] strings : pitomnik) {
            if (strings[0] != null) {
                if (strings[0].equals(dogname)) {
                    return true;
                }
            }
        }
        return false;
    }

    public  static String[][] getAllPuppies() {
        String[][] namePuppies = new String[getPuppyCount()][2];
        for (int i = 0; i < pitomnik.length; i++) {
            if (pitomnik[i][0] != null) {
                namePuppies[i][0] = pitomnik[i][0];
                namePuppies[i][1] = pitomnik[i][1];
            } else {
                return namePuppies;
            }
        }
        return namePuppies;
    }

    public static String[] getNamesByBreed(String poroda) {
        int numMatches = 0;
        for (String[] strings : pitomnik) {
            if (poroda.equals(strings[1])) {
                numMatches++;
            }
        }
        String[] dognames = new String[numMatches];
        int k = 0;
        for (String[] strings : pitomnik) {
            if (poroda.equals(strings[1])) {
                dognames[k++] = strings[0];
            }
        }
        return dognames;
    }
    public static void resetKennel() {
        pitomnik = new String[100][2];
    }

    public static void removePuppy(String dogname) {
        for (int i = 0; i < pitomnik.length - 2; i++) {
            if (dogname.equals(pitomnik[pitomnik.length - 1][0])) {
                pitomnik[pitomnik.length - 1][0] = null;
                pitomnik[pitomnik.length - 1][1] = null;
                return;
            }
            if (dogname.equals(pitomnik[i][0])) {
                pitomnik[i][0] = pitomnik[i + 1][0];
                pitomnik[i][1] = pitomnik[i + 1][1];
                for (int j = i + 1; j <= pitomnik.length - 2; j++) {
                    pitomnik[j][0] = pitomnik[j + 1][0];
                    pitomnik[j][1]  = pitomnik[j + 1][1];
                }
                pitomnik[pitomnik.length - 1][0] = null;
                pitomnik[pitomnik.length - 1][1] = null;
                return;
            } else {
                return;
            }
        }
    }
}


