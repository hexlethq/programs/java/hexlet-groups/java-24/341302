package exercise;

public class Dog {
    private String name;
    private String poroda;


    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Dog{" + "name='" + name + '\'' + ", poroda='" + poroda + '\'' + '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoroda() {
        return poroda;
    }

    public void setPoroda(String poroda) {
        this.poroda = poroda;
    }


    public Dog(String name, String poroda) {
        this.name = name;
        this.poroda = poroda;
    }


}
