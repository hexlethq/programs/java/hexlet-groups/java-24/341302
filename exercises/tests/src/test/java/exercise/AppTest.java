//package exercise;
//
//import java.util.Collections;
//import java.util.List;
//import java.util.Arrays;
//import java.util.ArrayList;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//class AppTest {
//    @Test
//    void testTake() {
//        // BEGIN
//        //1 right
//    List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
//    List<Integer> answer = new ArrayList<>(Arrays.asList(1,2));
//    int count = 2;
//    Assertions.assertThat(App.take(numbers1, count)).isEqualTo(answer); // => [1, 2]
//    Assertions.assertThat(App.take(numbers1, count)).isNotEmpty().startsWith(1).endsWith(2);
//    Assertions.assertThat(App.take(numbers1, count).size()).isEqualTo(2);
//
//       //wrong 1
//    List<Integer> numbers11 = new ArrayList<>(Collections.emptyList());
//    List<Integer> answer1 = new ArrayList<>();
//    int count1 = 2;
//    Assertions.assertThat(App.take(numbers11, count1)).isEqualTo(answer1); //
//    Assertions.assertThat(App.take(numbers11, count1)).isEmpty();
// //   Assertions.assertThat(App.take(numbers11, count1).size()).isEqualTo(null);
//
//      //wrong2
//     List<Integer> numbers111 = new ArrayList<>(Arrays.asList(1,2));
//     List<Integer> answer11 = new ArrayList<>(Arrays.asList(0, 100));
//     int count11 = 5;
//     Assertions.assertThat(App.take(numbers111, count11)).isEqualTo(answer11);
//     Assertions.assertThat(App.take(numbers111, count11)).isNotEmpty().startsWith(0).endsWith(100);
//     Assertions.assertThat(App.take(numbers111, count11).size()).isEqualTo(2);
//
//      //wrong3
//     List<Integer> numbers1111 = new ArrayList<>(Arrays.asList(0, 1, 3));
//     List<Integer> answer111 = new ArrayList<>(Arrays.asList(0, 1, 3 , 10));
//     int count111 = 3;
//     Assertions.assertThat(App.take(numbers1111, count111)).isEqualTo(answer111);
//     Assertions.assertThat(App.take(numbers1111, count111)).isNotEmpty().startsWith(0).endsWith(10);
//     Assertions.assertThat(App.take(numbers1111, count111).size()).isEqualTo(4);
//
//    }
//
//  @Test
//  void testTake1() {
//      List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
//      List<Integer> answer1 = new ArrayList<>(Arrays.asList(7, 3, 10));
//      int count = 8;
//      Assertions.assertThat(App.take(numbers2, count))
//              .isEqualTo(answer1)
//              .isNotEmpty()
//              .startsWith(7);
//
//   }
//}
