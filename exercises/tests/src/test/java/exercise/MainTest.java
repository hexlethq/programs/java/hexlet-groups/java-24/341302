package exercise;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MainTest {

    private   List<Integer> numbers1;
    private List<Integer> numbers2;
    private   int count1;
    private   int count2;

    @BeforeAll
    static void preparementTest() {
        System.out.println(" !!! Preparation Tests !!! ");
    }

    @BeforeEach
    void initTest() {
        this.numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        this.numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        this.count1 = 2;
        this.count2 = 8;
    }

    @Test
    void rightResultTest() {
        List<Integer> answer = new ArrayList<>(Arrays.asList(1, 2));
        Assertions.assertEquals(App.take(numbers1, count1), answer);
    }

    @Test
    void wrong1Test() {
        List<Integer> answer = new ArrayList<>();
        List<Integer> wrong1 = new ArrayList<>();
        Assertions.assertEquals(App.take(wrong1, count1), answer);
    }

    @Test
    void wrong2Test() {
        List<Integer> answer = new ArrayList<>(Arrays.asList(7, 3, 10));
        System.out.println(App.take(numbers2, count2));
        Assertions.assertEquals(App.take(numbers2, count2), answer);
    }

    @Test
    void wrong3Test() {
        List<Integer> answer = new ArrayList<>(Arrays.asList(7, 3));
        Assertions.assertEquals(App.take(numbers2, count1), answer);
    }
}
