package exercise;

import java.lang.reflect.Proxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private Map<String, String> beans = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            Inspect inspect = bean.getClass().getAnnotation(Inspect.class);
            this.beans.put(beanName, inspect.level());
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if (!beans.containsKey(beanName)) {
            return bean;
        }
        String logLvl = beans.get(beanName);
        String pattern = "Was called method: %s() with arguments: %s";
        Logger logger = LoggerFactory.getLogger(beanName);

        Object proxyInstance = Proxy.newProxyInstance(
                bean.getClass().getClassLoader(),
                bean.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    String msg = String.format(pattern, method.getName(), Arrays.toString(args));
                    if (logLvl.equals("info")) {
                        logger.info(msg);
                    } else {
                        logger.debug(msg);
                    }
                    return method.invoke(bean, args);
                });
        return proxyInstance;
    }

}
// END
