package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    static void init() {
        app = App.getApp();
        app.start();
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    static void destroy() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void createUser() {
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testNewCorrectUser() {
        HttpResponse<String> response = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "fname")
                .field("lastName", "lname")
                .field("email", "emailtest@mail.ru")
                .field("password", "999922222")
                .asString();

        User expectedUser = new QUser()
                .lastName.equalTo("lname")
                .findOne();

        assertThat(response.getStatus()).isEqualTo(302);
        assertThat(expectedUser.getFirstName()).isEqualTo("fname");
        assertThat(expectedUser.getLastName()).isEqualTo("lname");
        assertThat(expectedUser.getEmail()).isEqualTo("emailtest@mail.ru");
        assertThat(expectedUser.getPassword()).isEqualTo("999922222");
    }

    @Test
    void testNewIncorrectUser() {
        HttpResponse<String> response = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "")
                .field("lastName", "lname")
                .field("email", "mailmail@mail.ru")
                .field("password", "999992322")
                .asString();

        User expectedUser = new QUser()
                .email.equalTo("testmail@mail.ru")
                .findOne();

        String body = response.getBody();

        assertThat(response.getStatus()).isEqualTo(422);
        assertThat(expectedUser).isNull();
        assertThat(body).contains("lname");
        assertThat(body).contains("mailmail@mail.ru");
        assertThat(body).contains("Имя не должно быть пустым");
    }

    @Test
    void testNewIncorrectUserErrorsDisplaying() {
        HttpResponse<String> response = Unirest
                .post(baseUrl + "/users")
                .field("firstName", "")
                .field("lastName", "")
                .field("email", "")
                .field("password", "")
                .asString();

        String body = response.getBody();

        assertThat(body).contains("Имя не должно быть пустым");
        assertThat(body).contains("Фамилия не должна быть пустой");
        assertThat(body).contains("Должно быть валидным email");
        assertThat(body).contains("Пароль должен содержать не менее 4 символов");
    }

    // END
}
