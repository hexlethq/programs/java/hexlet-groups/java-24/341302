package exercise;

import java.util.Locale;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
      char[] symvols = sentence.toCharArray();
      if (symvols[symvols.length - 1] == 33) {
          System.out.println(sentence.toUpperCase(Locale.ROOT));
      } else {
          System.out.println(sentence.toLowerCase());
      }
        // END
    }
}
