package exercise;

import java.util.HashMap;
import java.util.Map;

public class App {

    public static HashMap<String, Integer> getWordCount(String str) {
        if (str.isEmpty()) {
            return new HashMap<>();
        }
        HashMap<String, Integer> first = new HashMap<>();
        String[] asd = str.split(" ");
        for (int i = 0; i < asd.length; i++) {
            int count = 0;
            for (int j = 0; j < asd.length; j++) {
                if (asd[i].equals(asd[j])) {
                    count++;
                }
            }
            first.put(asd[i], count);
        }
        return first;
    }

    public static String toString(HashMap<String, Integer> str1) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String, Integer> asd : str1.entrySet()) {

            stringBuilder.append("  ").append(asd.getKey()).append(":")
                    .append(" ").append(asd.getValue()).append('\n');

        }
        if ((stringBuilder + "").equals("")) {
            return "{}";
        }
        return "{" + '\n' + stringBuilder + "}";
    }

    public static void main(String[] args) {
        String sentence = "java is the best programming language java";
        HashMap wordsCount = App.getWordCount(sentence);
        System.out.println(wordsCount);

        String sentence1 = "the java is the best programming language java";
        HashMap wordsCount1 = App.getWordCount(sentence1);
        String result = App.toString(wordsCount1);
        System.out.println(result); // =>
// {
//   the: 2
//   java: 2
//   is: 1
//   best: 1
//   language: 1
//   programming: 1
    }


}

