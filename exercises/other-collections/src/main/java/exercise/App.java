package exercise;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;

class App {
    // BEGIN
    public static LinkedHashMap<String, String> genDiff(
                    Map<String, Object> data1, Map<String, Object> data2) {

        TreeSet<String> data1Keys = new TreeSet<>();
        TreeSet<String> data2Keys = new TreeSet<>();
        LinkedHashMap<String, String> result = new LinkedHashMap<>();

//key check
        for (HashMap.Entry<String, Object> entryKeyData2 : data2.entrySet()) {
            data2Keys.add(entryKeyData2.getKey());
        }
        for (HashMap.Entry<String, Object> entryKeyData1 : data1.entrySet()) {
            data1Keys.add(entryKeyData1.getKey());
        }

        for (String key1 : data1Keys) {
            for (String key2 : data2Keys) {
                if (key1.equals(key2)) {
                    break;
                }
            }
            result.put(key1, "deleted");
        }

        for (String key2 : data2Keys) {
            if (data1Keys.size() == 0) {
                result.put(key2, "added");
            }
            for (String key1 : data1Keys) {
                if (key2.equals(key1)) {
                    break;
                }
                result.put(key2, "added");
            }
        }
    // values check
        for (HashMap.Entry<String, Object> data1Entry : data1.entrySet()) {
            data1Keys.add(data1Entry.getKey());
            for (HashMap.Entry<String, Object> data2Entry : data2.entrySet()) {
                if (data1Entry.getKey().equals(data2Entry.getKey()) && data1Entry.getValue()
                        .equals(data2Entry.getValue())) {
                    result.put(data1Entry.getKey(), "unchanged");
                }
                if (data1Entry.getKey().equals(data2Entry.getKey())
                        && !(data1Entry.getValue().equals(data2Entry.getValue()))) {
                    result.put(data1Entry.getKey(), "changed");
                }
            }
        }
        return result;
    }

    //END
    public static void main(String[] args) {

    }
}
