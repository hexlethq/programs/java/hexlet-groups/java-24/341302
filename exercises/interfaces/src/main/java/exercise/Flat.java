package exercise;

// BEGIN
class Flat implements HomeInterface {

    private final double area;
    private final double balconyArea;
    private final int floor;

    Flat(double area, double balconyArea, int floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    @Override
    public double getArea() {
        return area + balconyArea;
    }

    @Override
    public String toString() {
        return "Flat square " + getArea() + " "
                + "meters on " + floor + " " + "floor";
    }

    @Override
    public int compareTo(HomeInterface homeInterface) {
        return Double.compare(this.getArea(), homeInterface.getArea());
    }
}

// END
