package exercise;

// BEGIN
class ReversedSequence implements CharSequence {

    private final String input;

    ReversedSequence(String string) {
        this.input = reverseString(string);
    }

    @Override
    public String toString() {
        return input;
    }

    @Override
    public int length() {
        return input.length();
    }

    @Override
    public char charAt(int i) {
        return input.charAt(i - 1);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return input.subSequence(i, i1);
    }

    public static String reverseString(String inputString) {
        StringBuilder result = new StringBuilder(inputString).reverse();
        return result.toString();
    }
}


