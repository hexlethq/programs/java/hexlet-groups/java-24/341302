package exercise;

// BEGIN
class Cottage implements HomeInterface {
    private final double area;
    private final int floorCount;

    Cottage(double square, int floors) {
        this.area = square;
        this.floorCount = floors;
    }

    @Override
    public String toString() {
        return floorCount + " floor cottage square " + area + " meters";
    }

    @Override
    public int compareTo(HomeInterface homeInterface) {
        return Double.compare(this.getArea(), homeInterface.getArea());
    }

    @Override
    public double getArea() {
        return area;
    }
}
// END
