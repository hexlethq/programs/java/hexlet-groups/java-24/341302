package exercise;

public interface HomeInterface extends Comparable<HomeInterface> {

    String toString();

    double getArea();

    int compareTo(HomeInterface homeInterface);
}
