package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static List<String> buildApartmentsList(
        List<HomeInterface> homeInterface, int n) {
        List<String> list = new ArrayList<>();
        if (0 == homeInterface.size()) {
            return list;
        }

        list = homeInterface.stream().sorted().limit(n)
                .map(HomeInterface::toString)
                .collect(Collectors.toList());
        return list;
    }
}
// END
