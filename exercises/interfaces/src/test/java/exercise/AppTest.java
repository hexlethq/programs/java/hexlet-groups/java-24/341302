package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import java.util.List;

class AppTest {

    @Test
    void testBuildAppartmentsList1() {
        List<HomeInterface> appartments = new ArrayList<>(List.of(
                new Flat(41, 3, 10),
                new Cottage(125.5, 2),
                new Flat(80, 10, 2),
                new Cottage(150, 3)
        ));

        List<String> expected = new ArrayList<>(List.of(
                "Flat square 44.0 meters on 10 floor",
                "Flat square 90.0 meters on 2 floor",
                "2 floor cottage square 125.5 meters"
        ));

        List<String> result = App.buildApartmentsList(appartments, 3);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void testBuildAppartmentsList2() {
        List<HomeInterface> appartments = new ArrayList<>(List.of(
                new Cottage(100, 1),
                new Flat(190, 10, 2),
                new Flat(180, 30, 5),
                new Cottage(250, 3)
        ));

        List<String> expected = new ArrayList<>(List.of(
                "1 floor cottage square 100.0 meters",
                "Flat square 200.0 meters on 2 floor",
                "Flat square 210.0 meters on 5 floor",
                "3 floor cottage square 250.0 meters"
        ));

        List<String> result = App.buildApartmentsList(appartments, 4);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void testBuildAppartmentsList3() {
        List<HomeInterface> appartments = new ArrayList<>();
        List<String> expected = new ArrayList<>();
        List<String> result = App.buildApartmentsList(appartments, 10);
        assertThat(result).isEqualTo(expected);
    }
    // BEGIN
    @Test
    void testCharSequense() {
        CharSequence text = new ReversedSequence("abcdef");
        assertThat(text.toString()).isEqualTo("fedcba");
        assertThat(text.charAt(2)).isEqualTo('e');
        assertThat(text.length()).isEqualTo(6);
        assertThat(text.subSequence(1, 4).toString()).isEqualTo("edc");
    }
    // END
}
