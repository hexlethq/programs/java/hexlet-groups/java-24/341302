package exercise;

public class NegativeRadiusException extends Exception {
    private final String message;

    public NegativeRadiusException(String s) {
        this.message = s;
    }

    public String getErrorDescription() {
        return message;
    }

}



