package exercise;

// BEGIN
class App extends Exception {

    public static void printSquare(Circle circle) {
        try {
            int square = (int) Math.round(circle.getSquare());
            System.out.println(square);
        } catch (NegativeRadiusException error) {
            if (error.getErrorDescription().equals("1")) {
                System.out.println("Can not calculate square");
            }
        }
        System.out.println("Calculation completed");
    }
}
// END
