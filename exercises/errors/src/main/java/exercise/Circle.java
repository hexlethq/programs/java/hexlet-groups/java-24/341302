package exercise;


// BEGIN
class Circle {
    private Point point;
    private final int radius;


    Circle(Point poin, int rad) {
        this.point = poin;
        this.radius = rad;
    }

    public int getRadius() {
        return this.radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (radius < 0)  {
            throw new NegativeRadiusException("1");
        }
        return 3.1415 * Math.pow(this.radius, 2);
    }

    public static void main(String[] args) {
        Point point = new Point(5, 7);
        Circle circle = new Circle(point, 10);
        App.printSquare(circle);
    }

}
// END
