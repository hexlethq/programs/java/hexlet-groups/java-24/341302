package exercise;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

class App {
    // BEGIN
   public static String buildList(String[] a) {
       if (a.length == 0) {
           return "";
       }
       StringBuilder stringBuilder = new StringBuilder();
       stringBuilder.append("<ul>\n");
       for (String s : a) {
           stringBuilder.append("  ").append("<li>").append(s).append("</li>\n");
       }
       return stringBuilder.append("</ul>") + "";
   }

    public static String  getUsersByYear(String[][] a, int num) throws ParseException {
        Calendar calendar = new GregorianCalendar();
        StringBuilder stringBuilder = new StringBuilder();
        for (String[] strings : a) {
            String str1 = strings[1];
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(str1);
            calendar.setTime(date1);
            int year1 = calendar.get(Calendar.YEAR);
            if (num == year1) {
                stringBuilder.append("  ").append("<li>").append(strings[0]).append("</li>\n");
            }
        }
        if (stringBuilder.length() == 0) {
            return "";
        }
        return  stringBuilder.insert(0, "<ul>\n").append("</ul>") + "";
    }

    public static String getYoungestUser(String[][] users, String dateOfBirth) throws ParseException {
        Date date = new SimpleDateFormat("dd MMM yyyy").parse(dateOfBirth);
        Date temp = new SimpleDateFormat("dd MMM yyyy").parse("10 Jun 1000");
        int index = -1;
       for (int i = 0; i < users.length; i++) {
           Date userYearOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(users[i][1]);
           if (userYearOfBirth.before(date) && userYearOfBirth.after(temp)) {
               temp = userYearOfBirth;
               index = i;
           }
       }
       if (index == -1) {
           return "";
       }
       return  "<ul>\n" + "  " + "<li>" + users[index][0] + "</li>\n" + "</ul>";
    }

    public static void main(String[] args) throws ParseException {

        String[] str = {"cats", "dogs", "birds"};
       System.out.println(buildList(str));

        String[][] users = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "1995-02-15"},
                {"Anna Sidorova", "1996-09-09"},
                {"John Smith", "1990-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
                {"Elsa Oscar", "2000-03-25"},
        };
    System.out.println(getUsersByYear(users, 1000));

        String[][] users1 = {
                {"Andrey Petrov", "1881-11-23"},
                {"Aleksey Ivanov", "2000-02-15"},
                {"Anna Sidorova", "1996-09-09"},
                {"John Smith", "1989-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
                {"Elsa Oscar", "1989-03-10"},
        };
       System.out.println(getYoungestUser(users1, "11 Jul 1900"));
    }
    // END
}
