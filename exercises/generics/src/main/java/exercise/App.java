package exercise;

import java.util.ArrayList;
import java.util.HashMap;

// BEGIN
public class App {
    public static ArrayList<HashMap> findWhere(ArrayList<HashMap<String, String>> books, HashMap<String, String> where) {
        ArrayList<HashMap> re = (ArrayList<HashMap>) books.clone();
        for (HashMap<String, String> book : books) {
            for (String keys : where.values()) {
            if (!book.containsValue(keys)) {
                    re.remove(book);
            }
            }
        }
        return re;
    }

    public static void main(String[] args) {
     // ArrayList<HashMap<String, String>> books = new ArrayList<>();

     // HashMap<String, String> book1 = new HashMap<>(
     //         Map.of("title", "Cymbeline", "author", "Shakespeare", "year", "1611")
     // );
     // HashMap<String, String> book2 = new HashMap<>(
     //         Map.of("title", "Book of Fooos", "author", "FooBar", "year", "1111")
     // );
     // HashMap<String, String> book3 = new HashMap<>(
     //         Map.of("title", "The Tempest", "author", "Shakespeare", "year", "1611")
     // );
     // HashMap<String, String> book4 = new HashMap<>(
     //         Map.of("title", "Book of Foos Barrrs", "author", "FooBar", "year", "2222")
     // );
     // HashMap<String, String> book5 = new HashMap<>(
     //         Map.of("title", "Still foooing", "author", "FooBar", "year", "3333")
     // );

     // books.add(book1);
     // books.add(book2);
     // books.add(book3);
     // books.add(book4);
     // books.add(book5);

     // HashMap<String, String> where = new HashMap<>(Map.of("author", "Shakespeare", "year", "1611"));

     // ArrayList<HashMap> result = App.findWhere(books, where);

     // System.out.println(result);
    }
}
