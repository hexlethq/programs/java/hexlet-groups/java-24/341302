package exercise;

import java.util.Arrays;
import java.util.stream.Stream;


public class App {

    // BEGIN
    public static String[][] enlargeArrayImage(String[][] imageinput) {
        String[][] result;
        result = Arrays.stream(imageinput)
                .flatMap(s -> Stream.of(s, s))
                .map(z -> Arrays.stream(z).flatMap(tt -> Stream.of(tt, tt)))
                .map(end -> end.toArray(String[]::new))
                .toArray(String[][]::new);
        return result;
    }

    public static void main(String[] args) {
        String[][] image = {
                {"*", "*", "*", "*"},
                {"*", " ", " ", "*"},
                {"*", " ", " ", "*"},
                {"*", "*", "*", "*"},
        };

        enlargeArrayImage(image);
        // enlargeArrayImage(image);
        //   System.out.println(Arrays.deepToString(image)); // =>
///
///  [*, *, *, *],
///  [*,  ,  , *],
///  [*,  ,  , *],
///  [*, *, *, *],
///

        // String[][] enlargedImage = App.enlargeArrayImage(image);
        ///  System.out.println(Arrays.deepToString(image)); // =>

///
///  [*, *, *, *, *, *, *, *],
///  [*, *, *, *, *, *, *, *],
///  [*, *,  ,  ,  ,  , *, *],
///  [*, *,  ,  ,  ,  , *, *],
///  [*, *,  ,  ,  ,  , *, *],
///  [*, *,  ,  ,  ,  , *, *],
///  [*, *, *, *, *, *, *, *],
///  [*, *, *, *, *, *, *, *],
///

    }
}
