package exercise;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testGetElements1() {
        int[] numbers1 = {};
        int[] result1 = App.getElementsLessAverage(numbers1);
        assertThat(result1).isEmpty();

        int[] numbers2 = {0, 1, 2, 3, 4, 5, 10, 12};
        int[] result2 = App.getElementsLessAverage(numbers2);
        int[] expected2 = {0, 1, 2, 3, 4};
        assertThat(result2).containsExactly(expected2);
    }

    // BEGIN
    @Test
    void testGetsum() {
        int[] numbers = {0, -5, 2, 3, 4};
        int result = App.getSumBeforeMinAndMax(numbers);
        int expected = 5;
        assertThat((result)).isEqualTo(expected);
    }

    @Test
    void testGetsum1() {
        int[] numbers3 = {0, 4, 2, 3, -4};
        int result3 = App.getSumBeforeMinAndMax(numbers3);
        int expected3 = 5;
        assertThat((result3)).isEqualTo(expected3);
    }
    @Test
    void testGetsum2() {
        int[] numbers3 = {0, -4, -2, -3, -8};
        int result3 = App.getSumBeforeMinAndMax(numbers3);
        int expected3 = -9;
        assertThat((result3)).isEqualTo(expected3);
    }
}   // END

