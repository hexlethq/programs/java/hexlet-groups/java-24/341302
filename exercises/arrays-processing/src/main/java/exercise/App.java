package exercise;
import java.util.Arrays;

class App {
    // BEGIN
    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) {
            return new int[]{};
        }
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        int aver =  sum / arr.length;
        int count = 0;
        for (int i : arr) {
            if (i <= aver) {
                count++;
            }
        }
        int[] newArr = new int[count];
        int index = 0;
        for (int j : arr) {
            if (j <= aver) {
                newArr[index] = j;
                index++;
            }
        }
        return newArr;
    }
    public  static int getSumBeforeMinAndMax(int[] arr) {
        int min = Integer.MAX_VALUE;
        int indexMin = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
                indexMin = i;
            }
        }
        int max = Integer.MIN_VALUE;
        int indexMax = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                indexMax = i;
            }
        }
        int sum = 0;
        if (indexMin < indexMax) {
            for (int i = indexMin + 1; i < indexMax; i++) {
                sum += arr[i];
            }
            return sum;
        } else {
            for (int i = indexMax + 1; i < indexMin; i++) {
                sum += arr[i];
            }
            return sum;
        }
    }
    public static void main(String[] args) {
        int[] arr =  {-1, -9, -2, -5};
        System.out.println(Arrays.toString(getElementsLessAverage(arr)));
      // System.out.println(getSumBeforeMinAndMax(arr));
    }
    // END
}
