package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


// BEGIN
public class App {
    public static boolean scrabble(String charString, String word) {
        word = word.toLowerCase(Locale.ROOT);
        char[] arr1 = charString.toCharArray();
        char[] wordArray = word.toCharArray();

        List<Character> wordList = new ArrayList<>();
        List<Character> charStringList = new ArrayList<>();

        for (int i = 0; i < wordArray.length; i++) {
            wordList.add(wordArray[i]);
        }
        for (int i = 0; i < arr1.length; i++) {
            charStringList.add(arr1[i]);
        }
        charStringList.retainAll(wordList);
        if (wordList.size() > charStringList.size()) {
            return false;
        }
        for (int i = 0; i < wordList.size(); i++) {
            for (int j = 0; j < charStringList.size(); j++) {
                if (wordList.get(i).equals(charStringList.get(j))) {
                    wordList.set(i, '0');
                    charStringList.set(j, '0');
                }
            }
        }

        for (int i = 0; i < wordList.size(); i++) {
            if (wordList.get(i) != '0') {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(scrabble("ajvayu", "Jva"));

    }
}
//END
