package exercise;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Integer> first = new ArrayList<>();
        first.add(1);
        first.add(2);
        first.add(3);
        first.add(4);
        first.add(2);
        List<Integer> second = new ArrayList<>();
        second.add(2);
        second.add(3);
        second.add(3);
        first.retainAll(second);
        second.removeAll(first);
        for (int i = 0; i < second.size(); i++) {
            System.out.println(second.get(i));
        }
        System.out.println();
        for (int i = 0; i < first.size(); i++) {
            System.out.println(first.get(i));
        }

    }
}
